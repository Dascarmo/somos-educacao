# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

["Biologia","Biologia marinha","Biomedicina","Botânica","Ciências Agrárias","Ciências Ambientais","Ciências Biológicas","Ciências da Saúde","Ciências do Meio Aquático","Ecologia","Educação física","Enfermagem","Fisioterapia","Fonoaudiologia","Gerontologia","Medicina","Medicina veterinária","Meteorologia","Microbiologia e Imunologia","Naturologia","Neurociência","Nutrição","Odontologia","Quiropraxia","Saúde Coletiva","Terapia ocupacional","Zootecnia"].each do |course_name|
    course = Course.find_or_create_by(name: course_name)
end