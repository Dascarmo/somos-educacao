class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :name, null: false
      t.integer :student_count, null: false, default: 0
      t.float :general_evaluation, null: false, default: 0

      t.timestamps
    end
  end
end
