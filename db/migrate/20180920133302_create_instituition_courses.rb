class CreateInstituitionCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :instituition_courses do |t|
      t.belongs_to :course, null: false, index: true, foreign_key: true
      t.belongs_to :instituition, null: false, index: true, foreign_key: true
      t.float :course_evaluation, null: false
      t.float :students_evaluation, null: false
      t.integer :students_count, null: false

      t.timestamps
    end
  end
end
