class CreateInstituitions < ActiveRecord::Migration[5.1]
  def change
    create_table :instituitions do |t|
      t.string :name, null: false
      t.float :general_evaluation, null: false, default: 0
      t.float :student_evaluation, null: false, default: 0

      t.timestamps
    end
  end
end
