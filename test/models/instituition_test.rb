require 'test_helper'

class InstituitionTest < ActiveSupport::TestCase
  test "should not save instituition without presence fields" do
    instituition = Instituition.new
    assert_not instituition.save
  end
  
end
