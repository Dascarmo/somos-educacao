require 'test_helper'

class InstituitionsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get instituitions_path
    assert_response :success
  end

  test "should create instituition" do
    assert_difference('Instituition.count') do
      post instituitions_path, params: {
        instituition: {
          name: "Universidade Federal do Rio de Janeiro"
        }
      }
    end
    assert_redirected_to instituitions_path
  end

  test "should update instituition name" do
    instituition = instituitions(:parana)
   
    patch instituition_url(instituition), params: { instituition: { name: "Universidade do Paraná" } }
   
    assert_redirected_to instituitions_path
    
    instituition.reload
    assert_equal "Universidade do Paraná", instituition.name
  end

  test "should add instituition course" do
    instituition = instituitions(:ceara)
    course = courses(:educacao_fisica)
   
    assert_difference('InstituitionCourse.count', +1) do
      patch instituition_url(instituition), params: {
        instituition: { 
          instituition_courses_attributes: [{
            course_id: course.id, 
            course_evaluation: 4, 
            students_evaluation: 3.5, 
            students_count: 35 
          }]
        } 
      }
    end
   
    assert_redirected_to instituitions_path
    
    instituition.reload
    course.reload
    assert_equal 4.25, instituition.general_evaluation
    assert_equal 3.75, instituition.student_evaluation
    assert_equal 4.5, course.general_evaluation
    assert_equal 85, course.student_count
  end

  test "should remove instituition course" do
    instituition = instituitions(:parana)
    course = courses(:biologia)
   
    assert_difference('InstituitionCourse.count', -1) do
      patch instituition_url(instituition), params: {
        instituition: { 
          instituition_courses_attributes: [{
            id: instituition_courses(:first).id, 
            _destroy: true
          }]
        } 
      }
    end
   
    assert_redirected_to instituitions_path
    
    instituition.reload
    course.reload
    assert_equal 5, instituition.general_evaluation
    assert_equal 4.5, instituition.student_evaluation
    assert_equal 4.5, course.general_evaluation
    assert_equal 33, course.student_count
  end

  test "should destroy instituition" do
    instituition = instituitions(:parana)
    assert_difference('Instituition.count', -1) do
      delete instituition_url(instituition)
    end
  
    assert_redirected_to instituitions_path
  end

end
