# README

This is a instruction manual to deploy the application locally. The requisits are:

* Ruby version

2.5.1

* System dependencies

 * Ubuntu operacional system;
 * MySQL;
 * Rails 5.0;
 * RVM (Ruby Gem Manager).

* Configuration

To start the application, first you need to install the ruby 2.5.1 version. After that, rum the command `gem install bundler` to install de bundler. Now, rum the command `bundle install` to get installed all the gems needed for the project.

* Database creation

To create the database, go to `config/database.yml` file and update the `default` configuration with the user and password database. After that, run the command `rails db:create db:migrate db:seed`. This will create the base schema, generate the tables and populate the courses table with a few examples of courses.

* How to run the test suite

To run the tests, the command `rails test` should do everything you'll need to check if the system are correctly deployed and operating.

* Deployment instructions

The deploy was made in Heroku, as demanded. Any question about it should be sent to the creator of this project.

* Deployed system

As requested, the system was deployed on Heroku cloud plataform. [Click here](https://pure-atoll-87632.herokuapp.com) to access. The public repository was uploaded to bitbucket and can be found [here](https://bitbucket.org/Dascarmo/somos-educacao).

## Considerations

This section will talk about the developed application.

This system was developed to consulting educational institutes by its degrees and rates. For that, three entities was created. They are: instituition, course and a intermediary table between them.

The instituition entity has the name of the isntituition, the general evaluation (average course evaluation of the courses of this instituition) and the student evaluation (average student evaluation).

The course entity has the name of the course, the general evaluation of the course and the number of students by all instituitions tha had this course.

And the intermediary table had the numbers of students of one course on one instituition, the evaluation of the course on this instituition, the student evaluation in the course on this instituition and the number of students in this course on this instituition.

### CRUD's

One CRUD (create, read, update and delete calls) was made (instituition's CRUD). That CRUD can be used to manage the present instituitions in the system.

Every time a institution's course is updated, added or removed, the numbers of the instituition changes. The course is updated two.

A bundle of courses was created for example to run the system. The CRUD of courses was not created because this is only a example of the real system, created only for testing pourposes.

### Unit Testing

Eight testing calls was made. They are:

* should not save instituition without presence fields:
Create just to not permit a institution be created without a name;

* should get index (courses):
Should return the index of courses.

* should get index (instituitions):
Should return the index of instituitions.

* should create instituition:
Should create a new instituition without problems.

* should update instituition name:
Should update a existing instituiton's name.

* should add instituition course:
Should add a instituition's course, updating correctly its numbers (like number of students and general evaluation).

* should remove instituition course:
Should remove a instituition's course, updating correctly its numbers (like number of students and general evaluation).

* should destroy instituition:
Should remove a instituition from the database.

