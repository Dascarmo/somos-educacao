class CoursesController < ApplicationController

    def index
        respond_to do |format|
            format.html
            format.json { render json: {data: Course.all} }
        end
    end
    
end
