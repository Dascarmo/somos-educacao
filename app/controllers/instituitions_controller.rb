class InstituitionsController < ApplicationController

    before_action :set_instituition, only: [:show, :edit, :update, :destroy]

    def index
        respond_to do |format|
            format.html
            format.json { render json: {data: Instituition.all.as_json(include:[:instituition_courses]) } }
        end
    end

    def show
        
    end

    def new
        @instituition = Instituition.new
    end

    def create
        @instituition = Instituition.new instituition_params
        if @instituition.save
            redirect_to instituitions_path
        else
            render :new
        end
    end

    def edit
        
    end

    def update
        if @instituition.update instituition_params
            redirect_to instituitions_path
        else
            render :edit
        end        
    end

    def destroy
        @instituition.destroy
        redirect_to instituitions_path
    end

    private

        def set_instituition
            @instituition = Instituition.find(params[:id])
        end
        
        def instituition_params
            params.require(:instituition).permit(:name, instituition_courses_attributes: [:id, :course_id, :course_evaluation, :students_evaluation, :students_count, :_destroy])
        end

end
