# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$.fn.dataTable.ext.search.push (settings, data, dataIndex) ->
  min = parseInt($('#evaluationMin').val(), 10)
  max = parseInt($('#evaluationMax').val(), 10)
  evaluation = parseFloat(data[2]) or 0
  # use data for the evaluation column
  if isNaN(min) and isNaN(max) or isNaN(min) and evaluation <= max or min <= evaluation and isNaN(max) or min <= evaluation and evaluation <= max
    return true
  false

$.fn.dataTable.ext.search.push (settings, data, dataIndex) ->
  min = parseInt($('#studentMin').val(), 10)
  max = parseInt($('#studentMax').val(), 10)
  student = parseFloat(data[3]) or 0
  # use data for the student column
  if isNaN(min) and isNaN(max) or isNaN(min) and student <= max or min <= student and isNaN(max) or min <= student and student <= max
    return true
  false

$.fn.dataTable.ext.search.push (settings, data, dataIndex) ->
  if settings.sTableId == 'instituitions'
    course_id = parseInt($('#course').val())
    courses_list = JSON.parse("["+data[4]+"]")
    # use data for the age column
    if isNaN(course_id) or courses_list.indexOf(course_id) > -1
        return true
    return false
  else
    true


$(document).on "turbolinks:load", ->
    instituitionsTable = $('.instituitions').DataTable
        ajax: '/instituitions.json'
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "general_evaluation"},
            {data: "student_evaluation"},
            {data: "instituition_courses[, ].course_id"}
        ]
        order: [[1, "asc"]]
        columnDefs: [
            {
                targets: [4],
                visible: false
            }
        ]
        autoWidth: true
        pagingType: 'full_numbers'
        processing: true
        serverSide: false
    $(".instituitions tbody").on "click", "tr", ->
        window.location.href = "/instituitions/"+instituitionsTable.row(this).data()["id"]+"/edit"
    $('#evaluationMin, #evaluationMax, #studentMin, #studentMax').keyup ->
        instituitionsTable.draw()
    $('#course').change ->
        instituitionsTable.draw()