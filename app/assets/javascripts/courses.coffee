# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
    coursesTable = $('.courses').DataTable
        ajax: '/courses.json'
        columns: [
            {data: "id"},
            {data: "name"},
            {data: "general_evaluation"},
            {data: "student_count"}
        ]
        order: [[0, "asc"]]
        autoWidth: true
        pagingType: 'full_numbers'
        processing: true
        serverSide: false
    # $(".courses tbody").on "click", "tr", ->
    #     console.log(coursesTable.row(this).data()["id"])
    