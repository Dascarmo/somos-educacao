class Course < ApplicationRecord

    has_many :instituition_courses
    has_many :instituitions, through: :instituition_courses

    scope :by_name, -> { order("name asc") }

    def update_course_rates!
        self.general_evaluation = instituition_courses.count > 0 ? (instituition_courses.sum(:course_evaluation).to_f/instituition_courses.count).round(2) : 0
        self.student_count = instituition_courses.sum(:students_count)
        self.save
    end

end
