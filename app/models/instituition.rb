class Instituition < ApplicationRecord

    has_many :instituition_courses, dependent: :destroy
    has_many :courses, through: :instituition_courses

    validates :name, presence: true

    scope :by_name, -> { order("name asc") }
    scope :by_evaluation, -> { order("evaluation asc") }

    accepts_nested_attributes_for :instituition_courses, allow_destroy: true, reject_if: :all_blank

    def update_instituition_rates!
        self.general_evaluation = instituition_courses.count != 0 ? (instituition_courses.sum(:course_evaluation).to_f/instituition_courses.count).round(2) : 0
        self.student_evaluation = instituition_courses.count != 0 ? (instituition_courses.sum(:students_evaluation).to_f/instituition_courses.count).round(2) : 0
        self.save
    end

end
