class InstituitionCourse < ApplicationRecord

    belongs_to :course
    belongs_to :instituition

    validates :instituition, :course, presence: true

    delegate :update_course_rates!, to: :course
    delegate :update_instituition_rates!, to: :instituition

    after_save :update_course_rates!
    after_save :update_instituition_rates!
    after_destroy :update_course_rates!
    after_destroy :update_instituition_rates!

end
